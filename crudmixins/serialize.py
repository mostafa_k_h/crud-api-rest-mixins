from crudmixins.models import Studentmodel
from rest_framework import serializers

class Studentserializer(serializers.ModelSerializer):
    class Meta:
        model = Studentmodel
        fields = "__all__"
