from django.db import models

class Studentmodel(models.Model):
    stid = models.IntegerField(primary_key=True)
    stname = models.CharField(max_length=100)
    stemail = models.CharField(max_length=100)
    stmobile = models.IntegerField()
    class Meta:
        db_table = "student"