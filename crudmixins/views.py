from crudmixins.models import Studentmodel
from crudmixins.serialize import Studentserializer
from rest_framework import mixins,generics


class Studentgetinsert(mixins.ListModelMixin,mixins.CreateModelMixin,generics.GenericAPIView):
    queryset = Studentmodel.objects.all()
    serializer_class = Studentserializer

    def get(self,request):
        return self.list(request)


    def get(self,request):
        return self.create(request)


class Studentupdatedel(mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin,generics.GenericAPIView):
    queryset = Studentmodel.objects.all()
    serializer_class = Studentserializer

    def get(self,request,pk):
        return self.retrieve(request,pk)
    
    def put(self,request,pk):
        return self.update(request,pk)

    def delete(self,request,pk):
        return self.delete(request,pk)



