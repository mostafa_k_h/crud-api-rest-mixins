
from django.contrib import admin
from django.urls import path,include
from . import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.Studentgetinsert.as_view()),
    path('student/<int:pk>', views.Studentupdatedel.as_view()),

]
